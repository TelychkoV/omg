export const modalWrapper = ` 
<div class="modal-wrapper">
<div class="modal">
  <div class="modal__content">
    <img
      class="modal__logo"
      src="https://flipdish.imgix.net/8kdMmFqPZT1f4wOg7uZCqKbUxB0.png"
      alt="Logo"
    />
  </div>
  <div class="modal__navigation"></div>
</div>

<div class="modal-settings">
  <h2 class="modal-settings__heading">Privacy settings</h2>
  <div class="modal-settings__content">
    <div id="firstBlock"></div>

    <div class="modal-settings__heading-wrapper">
      <h3 class="modal-settings__h3">Strictly necessary cookies</h3>
      <div class="header-input-block">
        <span class="header-input-block__label">Always On</span>
      </div>
    </div>
    <div id="secondBLock"></div>
    <div class="modal-settings__heading-wrapper">
      <h3 class="modal-settings__h3">Functional cookies</h3>
      <div class="header-input-block">
        <span class="header-input-block__label" id="checkboxLabelOne">Off</span>
        <label class="switch" tabindex="0">
          <input type="checkbox" id="first-ch">
          <span class="slider round"></span>
        </label>
          <div class="header-input-block__checkbox-label"></div>
        </label>
      </div>
    </div>
    <div id="thirdBLock"></div>
    <div class="modal-settings__heading-wrapper">
      <h3 class="modal-settings__h3">Performance cookies</h3>
      <div class="header-input-block">
        <span class="header-input-block__label" id="checkboxLabelTwo">Off</span>
        <label class="switch" tabindex="0">
          <input type="checkbox" id="second-ch">
          <span class="slider round"></span>
        </label>
      </div>
    </div>
    <div id="fourthBLock"></div>
    <div class="modal-settings__heading-wrapper">
      <h3 class="modal-settings__h3">Advertising cookies</h3>
      <div class="header-input-block">
        <span class="header-input-block__label" id="checkboxLabelThree">Off</span>
        <label class="switch" tabindex="0">
          <input type="checkbox" id="third-ch">
          <span class="slider round"></span>
        </label>
      </div>
    </div>
    <div id="fifthBLock"></div>
  </div>
  <div class="modal-settings__navigation"></div>
</div>
</div>
`;
