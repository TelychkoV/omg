const firstTextHeading = 'Your privacy settings';

const firstTextMainBeforeLink = `
    In order to give you the best experience, the [Restaurant name]
    website uses cookies and similar technologies for performance,
    analytics and to help the website function. Read our
`

const firstLinkValue = `Cookie Policy`;

const firstTextMainAfterLink = `
    to learn more and manage your preferences.
`;

const firstBtnText = 'Manage';
const secondBtnText = 'Accept';

export const firstLayerMarkUp = `
    <h2 class="modal__heading">${firstTextHeading}</h2>
    <p class="modal__text" tabindex="0">
        ${firstTextMainBeforeLink}
       <a class="modal__text_link" href="#">${firstLinkValue}</a>
       ${firstTextMainAfterLink}
    </p>
`;

export const firstLayerButtons = `
    <a 
        id="manage-btn" 
        class="modal__btn"
        tabindex="0">${firstBtnText}</a>
    <a id="accept-btn"  class="modal__btn" href="#" tabindex="0">${secondBtnText}</a>
`;