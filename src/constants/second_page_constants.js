const firstBlockText = '[Restaurant name] uses cookies and similar technologies to give you a better experience, enabling things like:';
const firstItem = 'basic site functions';
const secondItem = 'ensuring secure, safe transactions';
const thirdItem = 'secure account login';
const forthItem = 'remembering account, browser, and regional preferences';
const fivesItem = 'remembering privacy and security settings';
const sixItem = 'analysing site traffic and usage';


export const firstBlock = `
        <p class="modal-settings__text">
            ${firstBlockText}
          </p>
          <ul class="modal-settings__list">
            <li>${firstItem}</li>
            <li>${secondItem}</li>
            <li>${thirdItem}</li>
            <li>${forthItem}</li>
            <li>${fivesItem}</li>
            <li>${sixItem}</li>
          </ul>
          <p class="modal-settings__text">
            Detailed information can be found in the
            <a class="modal__text_link"
               href="https://www.flipdish.com/demo/cookie/cookie_consent.html#"
              >Privacy Policy</a
            >
            .
          </p>
`;

const secondBlockText = `Some of the technologies used are necessary for critical functions
like security, site integrity, account authentication, privacy
preferences, internal site usage, maintenance data and to make the
site work correctly for browsing and transactions.`;

export const secondBlock = `
          <p class="modal-settings__text">
            ${secondBlockText}
          </p>
          <p class="modal-settings__text">
          Read our <a class="modal__text_link" href="#">
          Cookie Policy
          </a>
          for more information.
          </p>
`;

const thirdBlockText = `Cookies and similar technologies are used to improve your experience, to do things like:`;
const thirdBlockFirstItem = `remember your login and general preferences`;

export const thirdBlock = ` <p class="modal-settings__text">
${thirdBlockText}
</p>
<ul class="modal-settings__list">
<li>${thirdBlockFirstItem}</li>
</ul>
<p class="modal-settings__text">
Read our
<a class="modal__text_link" href="#">
  Cookie Policy
</a>
for more information.
</p>
`;


const fourthBlockText = `These technologies are used for things like:`;
const fourthBlockFirstLi = `to understand usage via Google Analytics`;
const fourthBlockSecondLi = `to understand how you got to this website`;
const fourthBlockMoreText = `We do this with social media, marketing, and analytics partners (who
  may have their own information they’ve collected). Find out more in
  our`;

export const fourthBlock = `
<p class="modal-settings__text">
${fourthBlockText}
</p>
<ul class="modal-settings__list">
<li>${fourthBlockFirstLi}</li>
<li>${fourthBlockSecondLi}</li>
</ul>
<p class="modal-settings__text">
${fourthBlockMoreText}
<a class="modal__text_link" href="#">
  Cookie Policy </a
>.
</p>`;

const fifthBlockText = `These cookies are used for things like:`;
const fifthBlockFirstLi = `to deliver personalised advertisements to users who have already visited our website using Facebook Advertising`;
const fifthBlockMoreText = `We do this with social media, marketing, and analytics partners (who may have their own information they’ve collected). Find out more in our`;

export const fifthBlock = `
<p class="modal-settings__text">
${fifthBlockText}
</p>
<ul class="modal-settings__list">
<li>${fifthBlockFirstLi}</li>
</ul>
<p class="modal-settings__text">
${fifthBlockMoreText}
<a class="modal__text_link" href="#">
  Cookie Policy </a
>.
</p>
`;

const acceptAllBtn = `Accept all`;
const doneBtn = `Done`;

export const settingsNavigationLayout = `   
<a id='reject-Btn' class="modal__btn" href="#">${acceptAllBtn}</a>
<a id='done-Btn' class="modal__btn" href="#">${doneBtn}</a>`