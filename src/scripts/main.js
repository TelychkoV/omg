import { modalWrapper } from "../constants/modal_wrapper";
import {
  firstLayerMarkUp,
  firstLayerButtons,

} from "../constants/first_page_constants";

import {
  firstBlock,
  secondBlock,
  thirdBlock,
  fourthBlock,
  fifthBlock,
  settingsNavigationLayout

} from "../constants/second_page_constants";
import '../styles/index.scss';

// window.onload = function() {
//   window.fdCc.show();
// }

window.fdCc = {
  show: function () {
    document.body.innerHTML = modalWrapper;
    const modalBlock = document.querySelector('.modal');
    const modalContentFirst = document.querySelector('.modal__content');
    const settingsBlock = document.querySelector('.modal-settings');
    const modalNavigation = document.querySelector('.modal__navigation');
    const firstBlockElem = document.getElementById('firstBlock');
    const secondBLockElem = document.getElementById('secondBLock');
    const thirdBLockElem = document.getElementById('thirdBLock');
    const fourthBLockElem = document.getElementById('fourthBLock');
    const fifthBLockElem = document.getElementById('fifthBLock');
    const modalSettingsBtns = document.querySelector('.modal-settings__navigation');

    modalContentFirst.innerHTML += firstLayerMarkUp;
    modalNavigation.innerHTML = firstLayerButtons;
    firstBlockElem.innerHTML = firstBlock;
    secondBLockElem.innerHTML = secondBlock;
    thirdBLockElem.innerHTML = thirdBlock;
    fourthBLockElem.innerHTML = fourthBlock;
    fifthBLockElem.innerHTML = fifthBlock;
    modalSettingsBtns.innerHTML = settingsNavigationLayout;

    const manageBtn = document.getElementById('manage-btn');
    const acceptBtn = document.getElementById('accept-btn');
    const acceptAllBtn = document.getElementById('reject-Btn');
    const doneBtn = document.getElementById('done-Btn');
    const firstCheckBox = document.getElementById('first-ch');
    const secondCheckBox = document.getElementById('second-ch');
    const thirdCheckBox = document.getElementById('third-ch');
    const checkboxLabelOne = document.getElementById('checkboxLabelOne');
    const checkboxLabelTwo = document.getElementById('checkboxLabelTwo');
    const checkboxLabelThree = document.getElementById('checkboxLabelThree');

    const toggleModalPage = () => {
      modalBlock.classList.add('modal_hidden');
      settingsBlock.classList.add('modal-settings_visible');
    };


    const handleManageBtnClick = () => {
      toggleModalPage();
    };

    const handleEnterClick = (e) => {
      if (e.keyCode === 13) {
        toggleModalPage();
      }
    };

    const handleAcceptClick = () => {
      modalBlock.classList.add('modal_hidden');
      this.hide();
    };

    const handleDoneClick = () => {
      settingsBlock.classList.remove('modal-settings_visible');
      this.hide();
    };

    // Methods to manipulate checkboxes states
    const handleAcceptAllClick = () => {
      firstCheckBox.checked = true;
      secondCheckBox.checked = true;
      thirdCheckBox.checked = true;
      checkboxLabelOne.innerText = 'On';
      checkboxLabelTwo.innerText = 'On';
      checkboxLabelThree.innerText = 'On';
    };

    // Methods to manipulate checkboxes states
    const handleFirstCheckboxClick = () => {
      if (!firstCheckBox.checked) {
        checkboxLabelOne.innerText = 'Off';
      } else {
        checkboxLabelOne.innerText = 'On';
      }
    };

    const handlesecondCheckBoxClick = () => {
      if (!secondCheckBox.checked) {
        checkboxLabelTwo.innerText = 'Off';
      } else {
        checkboxLabelTwo.innerText = 'On';
      }
    };

    const handlethirdCheckBoxClick = () => {
      if (!thirdCheckBox.checked) {
        checkboxLabelThree.innerText = 'Off';
      } else {
        checkboxLabelThree.innerText = 'On';
      }
    }

    manageBtn.addEventListener('click', handleManageBtnClick);
    manageBtn.addEventListener('keyup', handleEnterClick);
    acceptBtn.addEventListener('click', handleAcceptClick);
    acceptAllBtn.addEventListener('click', handleAcceptAllClick);
    doneBtn.addEventListener('click', handleDoneClick);
    firstCheckBox.addEventListener('click', handleFirstCheckboxClick);
    secondCheckBox.addEventListener('click', handlesecondCheckBoxClick);
    thirdCheckBox.addEventListener('click', handlethirdCheckBoxClick);

  },

  hide: function () {
    document.querySelector('.modal-wrapper').remove();
  },
};
